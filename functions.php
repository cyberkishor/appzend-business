<?php
/**
 * Describe child theme functions
 *
 * @package Appzend
 * @subpackage Appzend Business
 * 
 */

 if ( ! function_exists( 'appzend_business_setup' ) ) :

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on Appzend Business, use a find and replace
     * to change 'appzend-business' to the name of your theme in all the template files.
    */
    load_theme_textdomain( 'appzend-business', get_template_directory() . '/languages' );

    add_theme_support( "title-tag" );
    add_theme_support( 'automatic-feed-links' );
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function appzend_business_setup() {
        
        $appzend_business_theme_info = wp_get_theme();
        $GLOBALS['appzend_business_version'] = $appzend_business_theme_info->get( 'Version' );
    }
endif;
add_action( 'after_setup_theme', 'appzend_business_setup' );


/**
 * Enqueue child theme styles and scripts
*/
function appzend_business_scripts() {
    
    global $appzend_business_version;
    wp_enqueue_style( 'appzend-parent-style', trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'style.css', array(), esc_attr( $appzend_business_version ) );
    
    wp_enqueue_style( 'appzend-business-style', get_stylesheet_uri(), esc_attr( $appzend_business_version ) );
}
add_action( 'wp_enqueue_scripts', 'appzend_business_scripts', 20 );


if ( ! function_exists( 'appzend_business_child_options' ) ) {

    function appzend_business_child_options( $wp_customize ) {
        
        /** page width in % */
        $wp_customize->add_setting("appzend_business_page_width", array(
            'sanitize_callback' => 'appzend_sanitize_number_blank',
            'default' => 90,
            'transport' => 'postMessage'
        ));
        $wp_customize->add_setting("appzend_business_page_width_tablet", array(
            'sanitize_callback' => 'appzend_sanitize_number_blank',
            'default' => '100',
            'transport' => 'postMessage'
        ));
        $wp_customize->add_setting("appzend_business_page_width_mobile", array(
            'sanitize_callback' => 'appzend_sanitize_number_blank',
            'default' => '100',
            'transport' => 'postMessage'
        ));
        $wp_customize->add_control(new AppZend_Range_Slider_Control($wp_customize, "appzend_business_page_width_group", array(
            'section' => "appzend_site_layout_section",
            'label' => esc_html__('Width (%)', 'appzend-business'),
            'input_attrs' => array(
                'min' => 70,
                'max' => 100,
                'step' => 1,
            ),
            'settings' => array(
                'desktop' => "appzend_business_page_width",
                'tablet' => "appzend_business_page_width_tablet",
                'mobile' => "appzend_business_page_width_mobile",
            )
        )));



        /** header call button and icon */
        $wp_customize->add_setting('appzend_call_button', array(
            'default' => 'disable',
            'sanitize_callback' => 'appzend_sanitize_switch',	
        ));

        $wp_customize->add_control(new AppZend_Switch_Control($wp_customize, 'appzend_call_button', array(
            'label' => esc_html__('Enable Call Section', 'appzend-business'),
            'section' => 'appzend_header_settings',
            'switch_label' => array(
                'enable' => esc_html__('Yes', 'appzend-business'),
                'disable' => esc_html__('No', 'appzend-business'),
            ),
        )));

        /*Button Text*/
        $wp_customize->add_setting( 'appzend-call-button-text',
            array(
                'default'           => esc_html__("Call Anytime", 'appzend-business'),
                'sanitize_callback' => 'sanitize_text_field',
            )
        );
        $wp_customize->add_control( 'appzend-call-button-text',
            array(
                'label'    => esc_html__( 'Call Text', 'appzend-business' ),
                'section'         => 'appzend_header_settings',
                'settings' => 'appzend-call-button-text',
                'type'     => 'text',
            )
        );

        $wp_customize->add_setting( 'appzend-call-phone-text',
            array(
                'default'           => esc_html__("+1 000 000", 'appzend-business'),
                'sanitize_callback' => 'sanitize_text_field',
            )
        );
        $wp_customize->add_control( 'appzend-call-phone-text',
            array(
                'label'    => esc_html__( 'Phone Number', 'appzend-business' ),
                'section'         => 'appzend_header_settings',
                'settings' => 'appzend-call-phone-text',
                'type'     => 'text',
            )
        );
        
        $wp_customize->add_setting( 'appzend-call-phone-icon',
            array(
                'default'           => 'far fa-comments',
                'sanitize_callback' => 'sanitize_text_field',
            )
        );
        $wp_customize->add_control( new AppZend_Fontawesome_Icons($wp_customize, 'appzend-call-phone-icon',
            array(
                'label'    => esc_html__( 'Phone Number', 'appzend-business' ),
                'section'         => 'appzend_header_settings',
                'settings' => 'appzend-call-phone-icon',
                'type'     => 'icon',
            )
        ));

    }
}
add_action( 'customize_register' , 'appzend_business_child_options', 11 );

/**
 * Dynamic Style from parent
 */
add_filter( 'appzend_dynamic_css', 'appzend_business_dynamic_css', 100 );
function appzend_business_dynamic_css($dynamic_css){
    
    $appzend_business_dynamic_css = $appzend_business_tablet_css = $appzend_business_mobile_css = "";
    $appzend_business_page_width = get_theme_mod('appzend_business_page_width', 90);
    $appzend_business_page_width_tablet = get_theme_mod('appzend_business_page_width_tablet', 100);
    $appzend_business_page_width_mobile = get_theme_mod('appzend_business_page_width_mobile', 100);

    $appzend_business_dynamic_css .="
        .container{
            max-width: {$appzend_business_page_width}%;
        }
    ";
    $appzend_business_tablet_css .="
        .container{
            max-width: {$appzend_business_page_width_tablet}%;
        }
    ";
    
    $appzend_business_mobile_css .="
        .container{
            max-width: {$appzend_business_page_width_mobile}%;
        }
    ";

    $nav_text_color = get_theme_mod('appzend_menu_text_color');
    if($nav_text_color) $appzend_business_dynamic_css .=" .nav-menu .link-box .call-us * {color: $nav_text_color}";


    $appzend_business_dynamic_css .= "@media screen and (max-width:768px){{$appzend_business_tablet_css}}";
    $appzend_business_dynamic_css .= "@media screen and (max-width:480px){{$appzend_business_mobile_css}}";

    $dynamic_css .= $appzend_business_dynamic_css;

    wp_add_inline_style( 'appzend-business-style', appzend_business_strip_whitespace($dynamic_css) );

}

function appzend_business_strip_whitespace($css) {
    $replace = array(
        "#/\*.*?\*/#s" => "", // Strip C style comments.
        "#\s\s+#" => " ", // Strip excess whitespace.
    );
    $search = array_keys($replace);
    $css = preg_replace($search, $replace, $css);

    $replace = array(
        ": " => ":",
        "; " => ";",
        " {" => "{",
        " }" => "}",
        ", " => ",",
        "{ " => "{",
        ";}" => "}", // Strip optional semicolons.
        ",\n" => ",", // Don't wrap multiple selectors.
        "\n}" => "}", // Don't wrap closing braces.
        "} " => "}", // Put each rule on it's own line.
    );
    $search = array_keys($replace);
    $css    = str_replace($search, $replace, $css);

    return trim($css);
}

/**
 * Enqueue required scripts/styles for customizer panel
 *
 * @since 1.0.0
 *
 */
function appzend_business_customize_scripts(){
	wp_enqueue_script('appzend-business-customizer', get_template_directory_uri() . '/assets/js/admin.js', array('jquery', 'customize-controls'), true);
}
add_action('customize_controls_enqueue_scripts', 'appzend_business_customize_scripts');